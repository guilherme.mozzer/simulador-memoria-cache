﻿
/*
 * CLASSE FILA - IMPLEMENTA O TIPO ABSTRATO DE DADOS FILA ESTÁTICA CIRCULAR
 * OPERAÇÕES: *
 *	vazia() -> retorna true se a fila está vazia.
 *	cheia() -> returna true se a fila  está cheia.
 *	enfileirar(x) -> coloca o elemento x no fim da fila.
 *	desenfileirar() -> retorna o elemento situado no inicio da fila.
 */

namespace SimuladorMC_AOC
{
    public class Fila
    {
        private static Fila myInstance;
        private int[] vet;
        private int inicio;
        private int fim;

        private Fila(int tam)
        {
            vet = new int[tam];
            inicio = fim = 0;
        }

        // com o construtor private não é possível instanciar a classe com o operador new de fora da classe
        public static Fila getInstance(int tam)
        {
            if (myInstance == null)
            {
                myInstance = new Fila(tam);
            }
            return myInstance;
        }

        /*public Fila(int tam)
        {
                vet = new int[tam];
                inicio = fim = 0;
        }*/

        public void enfileirar(int i)
        {
            vet[fim] = i;
            fim = (fim + 1) % vet.Length;
        }

        public int desenfileirar()
        {
            int item;
            item = vet[inicio];
            inicio = (inicio + 1) % vet.Length;
            return item;
        }

        public bool vazia()
        {
            return inicio == fim;
        }

        public bool cheia()
        {
            return ((fim+1) % vet.Length) == inicio;
        }

        public static Fila Destruir()
        {
            return myInstance = null;
        }
    }
}
