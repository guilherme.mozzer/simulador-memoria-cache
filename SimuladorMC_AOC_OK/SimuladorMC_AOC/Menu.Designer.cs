﻿namespace SimuladorMC_AOC
{
    partial class Formmenu
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Formmenu));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grbentradas = new System.Windows.Forms.GroupBox();
            this.gpbAS = new System.Windows.Forms.GroupBox();
            this.rbnFIFO = new System.Windows.Forms.RadioButton();
            this.rbnLFU = new System.Windows.Forms.RadioButton();
            this.rbnLRU = new System.Windows.Forms.RadioButton();
            this.bntCancelar = new System.Windows.Forms.Button();
            this.gpbTM = new System.Windows.Forms.GroupBox();
            this.rbnAssociativa = new System.Windows.Forms.RadioButton();
            this.rbnDireta = new System.Windows.Forms.RadioButton();
            this.bntOk = new System.Windows.Forms.Button();
            this.txtQtdLinha = new System.Windows.Forms.TextBox();
            this.txtTBloco = new System.Windows.Forms.TextBox();
            this.txtTMP = new System.Windows.Forms.TextBox();
            this.lbllinha = new System.Windows.Forms.Label();
            this.lblBloco = new System.Windows.Forms.Label();
            this.lblMP = new System.Windows.Forms.Label();
            this.lblSequencia = new System.Windows.Forms.Label();
            this.txtAddBloco = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvMP = new System.Windows.Forms.DataGridView();
            this.Blocos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMC = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BlocosMC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bntAddBloco = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.grbentradas.SuspendLayout();
            this.gpbAS.SuspendLayout();
            this.gpbTM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMC)).BeginInit();
            this.SuspendLayout();
            // 
            // grbentradas
            // 
            this.grbentradas.Controls.Add(this.gpbAS);
            this.grbentradas.Controls.Add(this.bntCancelar);
            this.grbentradas.Controls.Add(this.gpbTM);
            this.grbentradas.Controls.Add(this.bntOk);
            this.grbentradas.Controls.Add(this.txtQtdLinha);
            this.grbentradas.Controls.Add(this.txtTBloco);
            this.grbentradas.Controls.Add(this.txtTMP);
            this.grbentradas.Controls.Add(this.lbllinha);
            this.grbentradas.Controls.Add(this.lblBloco);
            this.grbentradas.Controls.Add(this.lblMP);
            this.grbentradas.Location = new System.Drawing.Point(12, 25);
            this.grbentradas.Name = "grbentradas";
            this.grbentradas.Size = new System.Drawing.Size(226, 351);
            this.grbentradas.TabIndex = 0;
            this.grbentradas.TabStop = false;
            // 
            // gpbAS
            // 
            this.gpbAS.Controls.Add(this.rbnFIFO);
            this.gpbAS.Controls.Add(this.rbnLFU);
            this.gpbAS.Controls.Add(this.rbnLRU);
            this.gpbAS.Location = new System.Drawing.Point(9, 215);
            this.gpbAS.Name = "gpbAS";
            this.gpbAS.Size = new System.Drawing.Size(203, 90);
            this.gpbAS.TabIndex = 4;
            this.gpbAS.TabStop = false;
            this.gpbAS.Text = "Algoritimo de Substituição";
            // 
            // rbnFIFO
            // 
            this.rbnFIFO.AutoSize = true;
            this.rbnFIFO.Location = new System.Drawing.Point(6, 19);
            this.rbnFIFO.Name = "rbnFIFO";
            this.rbnFIFO.Size = new System.Drawing.Size(130, 17);
            this.rbnFIFO.TabIndex = 2;
            this.rbnFIFO.TabStop = true;
            this.rbnFIFO.Text = "FIFO (First in, First out)";
            this.rbnFIFO.UseVisualStyleBackColor = true;
            // 
            // rbnLFU
            // 
            this.rbnLFU.AutoSize = true;
            this.rbnLFU.Location = new System.Drawing.Point(6, 42);
            this.rbnLFU.Name = "rbnLFU";
            this.rbnLFU.Size = new System.Drawing.Size(160, 17);
            this.rbnLFU.TabIndex = 1;
            this.rbnLFU.TabStop = true;
            this.rbnLFU.Text = "LFU (Least Frequently Used)";
            this.rbnLFU.UseVisualStyleBackColor = true;
            // 
            // rbnLRU
            // 
            this.rbnLRU.AutoSize = true;
            this.rbnLRU.Location = new System.Drawing.Point(6, 65);
            this.rbnLRU.Name = "rbnLRU";
            this.rbnLRU.Size = new System.Drawing.Size(155, 17);
            this.rbnLRU.TabIndex = 0;
            this.rbnLRU.TabStop = true;
            this.rbnLRU.Text = "LRU (Least Recently Used)";
            this.rbnLRU.UseVisualStyleBackColor = true;
            // 
            // bntCancelar
            // 
            this.bntCancelar.BackColor = System.Drawing.Color.Tomato;
            this.bntCancelar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bntCancelar.BackgroundImage")));
            this.bntCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.bntCancelar.ForeColor = System.Drawing.Color.White;
            this.bntCancelar.Location = new System.Drawing.Point(6, 308);
            this.bntCancelar.Name = "bntCancelar";
            this.bntCancelar.Size = new System.Drawing.Size(88, 37);
            this.bntCancelar.TabIndex = 0;
            this.bntCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bntCancelar.UseVisualStyleBackColor = false;
            this.bntCancelar.Click += new System.EventHandler(this.bntCancelar_Click);
            // 
            // gpbTM
            // 
            this.gpbTM.Controls.Add(this.rbnAssociativa);
            this.gpbTM.Controls.Add(this.rbnDireta);
            this.gpbTM.Location = new System.Drawing.Point(9, 136);
            this.gpbTM.Name = "gpbTM";
            this.gpbTM.Size = new System.Drawing.Size(203, 73);
            this.gpbTM.TabIndex = 3;
            this.gpbTM.TabStop = false;
            this.gpbTM.Text = "*Tecnica de Mapeamento";
            // 
            // rbnAssociativa
            // 
            this.rbnAssociativa.AutoSize = true;
            this.rbnAssociativa.Location = new System.Drawing.Point(6, 43);
            this.rbnAssociativa.Name = "rbnAssociativa";
            this.rbnAssociativa.Size = new System.Drawing.Size(79, 17);
            this.rbnAssociativa.TabIndex = 1;
            this.rbnAssociativa.TabStop = true;
            this.rbnAssociativa.Text = "Associativa";
            this.rbnAssociativa.UseVisualStyleBackColor = true;
            // 
            // rbnDireta
            // 
            this.rbnDireta.AutoSize = true;
            this.rbnDireta.Location = new System.Drawing.Point(6, 19);
            this.rbnDireta.Name = "rbnDireta";
            this.rbnDireta.Size = new System.Drawing.Size(53, 17);
            this.rbnDireta.TabIndex = 0;
            this.rbnDireta.TabStop = true;
            this.rbnDireta.Text = "Direta";
            this.rbnDireta.UseVisualStyleBackColor = true;
            // 
            // bntOk
            // 
            this.bntOk.BackColor = System.Drawing.Color.SpringGreen;
            this.bntOk.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bntOk.BackgroundImage")));
            this.bntOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.bntOk.ForeColor = System.Drawing.Color.Black;
            this.bntOk.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bntOk.Location = new System.Drawing.Point(117, 308);
            this.bntOk.Name = "bntOk";
            this.bntOk.Size = new System.Drawing.Size(95, 37);
            this.bntOk.TabIndex = 5;
            this.bntOk.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bntOk.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.bntOk.UseVisualStyleBackColor = false;
            this.bntOk.Click += new System.EventHandler(this.bntOk_Click);
            // 
            // txtQtdLinha
            // 
            this.txtQtdLinha.Location = new System.Drawing.Point(9, 110);
            this.txtQtdLinha.MaxLength = 20;
            this.txtQtdLinha.Name = "txtQtdLinha";
            this.txtQtdLinha.Size = new System.Drawing.Size(138, 20);
            this.txtQtdLinha.TabIndex = 2;
            this.txtQtdLinha.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQtdLinha_KeyPress);
            // 
            // txtTBloco
            // 
            this.txtTBloco.Location = new System.Drawing.Point(9, 71);
            this.txtTBloco.MaxLength = 20;
            this.txtTBloco.Name = "txtTBloco";
            this.txtTBloco.Size = new System.Drawing.Size(138, 20);
            this.txtTBloco.TabIndex = 1;
            this.txtTBloco.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTBloco_KeyPress);
            // 
            // txtTMP
            // 
            this.txtTMP.BackColor = System.Drawing.SystemColors.Window;
            this.txtTMP.Location = new System.Drawing.Point(9, 32);
            this.txtTMP.MaxLength = 20;
            this.txtTMP.Name = "txtTMP";
            this.txtTMP.Size = new System.Drawing.Size(138, 20);
            this.txtTMP.TabIndex = 0;
            this.txtTMP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTMP_KeyPress);
            // 
            // lbllinha
            // 
            this.lbllinha.AutoSize = true;
            this.lbllinha.Location = new System.Drawing.Point(6, 94);
            this.lbllinha.Name = "lbllinha";
            this.lbllinha.Size = new System.Drawing.Size(210, 13);
            this.lbllinha.TabIndex = 2;
            this.lbllinha.Text = "*Quantidade de Linhas da Memória Cache:";
            // 
            // lblBloco
            // 
            this.lblBloco.AutoSize = true;
            this.lblBloco.Location = new System.Drawing.Point(6, 55);
            this.lblBloco.Name = "lblBloco";
            this.lblBloco.Size = new System.Drawing.Size(156, 13);
            this.lblBloco.TabIndex = 1;
            this.lblBloco.Text = "*Tamanho do Bloco (em Bytes):";
            // 
            // lblMP
            // 
            this.lblMP.AutoSize = true;
            this.lblMP.Location = new System.Drawing.Point(6, 16);
            this.lblMP.Name = "lblMP";
            this.lblMP.Size = new System.Drawing.Size(212, 13);
            this.lblMP.TabIndex = 0;
            this.lblMP.Text = "*Tamanho da Memória Principal (em Bytes):";
            // 
            // lblSequencia
            // 
            this.lblSequencia.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.lblSequencia.AutoSize = true;
            this.lblSequencia.Location = new System.Drawing.Point(244, 67);
            this.lblSequencia.Name = "lblSequencia";
            this.lblSequencia.Size = new System.Drawing.Size(94, 13);
            this.lblSequencia.TabIndex = 8;
            this.lblSequencia.Text = "Blocos mapeados:";
            // 
            // txtAddBloco
            // 
            this.txtAddBloco.Location = new System.Drawing.Point(244, 41);
            this.txtAddBloco.MaxLength = 6;
            this.txtAddBloco.Name = "txtAddBloco";
            this.txtAddBloco.Size = new System.Drawing.Size(143, 20);
            this.txtAddBloco.TabIndex = 6;
            this.txtAddBloco.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAddBloco_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(244, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Bloco a ser indexado na MC:";
            // 
            // dgvMP
            // 
            this.dgvMP.AllowUserToAddRows = false;
            this.dgvMP.AllowUserToDeleteRows = false;
            this.dgvMP.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMP.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgvMP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMP.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Blocos});
            this.dgvMP.Location = new System.Drawing.Point(244, 83);
            this.dgvMP.Name = "dgvMP";
            this.dgvMP.ReadOnly = true;
            this.dgvMP.Size = new System.Drawing.Size(179, 293);
            this.dgvMP.TabIndex = 9;
            // 
            // Blocos
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Blocos.DefaultCellStyle = dataGridViewCellStyle1;
            this.Blocos.HeaderText = "Memória Principal";
            this.Blocos.Name = "Blocos";
            this.Blocos.ReadOnly = true;
            // 
            // dgvMC
            // 
            this.dgvMC.AllowUserToAddRows = false;
            this.dgvMC.AllowUserToDeleteRows = false;
            this.dgvMC.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMC.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgvMC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMC.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.BlocosMC});
            this.dgvMC.Location = new System.Drawing.Point(429, 83);
            this.dgvMC.Name = "dgvMC";
            this.dgvMC.ReadOnly = true;
            this.dgvMC.Size = new System.Drawing.Size(196, 293);
            this.dgvMC.TabIndex = 10;
            // 
            // Id
            // 
            this.Id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Id.DefaultCellStyle = dataGridViewCellStyle2;
            this.Id.FillWeight = 50.76142F;
            this.Id.HeaderText = "Id";
            this.Id.MaxInputLength = 10;
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            // 
            // BlocosMC
            // 
            this.BlocosMC.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.BlocosMC.DefaultCellStyle = dataGridViewCellStyle3;
            this.BlocosMC.FillWeight = 149.2386F;
            this.BlocosMC.HeaderText = "Memória Cache";
            this.BlocosMC.Name = "BlocosMC";
            this.BlocosMC.ReadOnly = true;
            // 
            // bntAddBloco
            // 
            this.bntAddBloco.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.bntAddBloco.BackColor = System.Drawing.Color.White;
            this.bntAddBloco.Enabled = false;
            this.bntAddBloco.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntAddBloco.Location = new System.Drawing.Point(393, 31);
            this.bntAddBloco.Name = "bntAddBloco";
            this.bntAddBloco.Size = new System.Drawing.Size(76, 30);
            this.bntAddBloco.TabIndex = 7;
            this.bntAddBloco.Text = "Adicionar";
            this.bntAddBloco.UseVisualStyleBackColor = false;
            this.bntAddBloco.Click += new System.EventHandler(this.bntAddBloco_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "* Campos Obrigatórios";
            // 
            // Formmenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.Azure;
            this.ClientSize = new System.Drawing.Size(635, 382);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bntAddBloco);
            this.Controls.Add(this.dgvMC);
            this.Controls.Add(this.dgvMP);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtAddBloco);
            this.Controls.Add(this.lblSequencia);
            this.Controls.Add(this.grbentradas);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Formmenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Simulador de Mapeamento de Dados da Memória Cache";
            this.grbentradas.ResumeLayout(false);
            this.grbentradas.PerformLayout();
            this.gpbAS.ResumeLayout(false);
            this.gpbAS.PerformLayout();
            this.gpbTM.ResumeLayout(false);
            this.gpbTM.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMC)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grbentradas;
        private System.Windows.Forms.GroupBox gpbAS;
        private System.Windows.Forms.RadioButton rbnLFU;
        private System.Windows.Forms.RadioButton rbnLRU;
        private System.Windows.Forms.GroupBox gpbTM;
        private System.Windows.Forms.RadioButton rbnAssociativa;
        private System.Windows.Forms.RadioButton rbnDireta;
        private System.Windows.Forms.TextBox txtQtdLinha;
        private System.Windows.Forms.TextBox txtTBloco;
        private System.Windows.Forms.TextBox txtTMP;
        private System.Windows.Forms.Label lbllinha;
        private System.Windows.Forms.Label lblBloco;
        private System.Windows.Forms.Label lblMP;
        private System.Windows.Forms.Button bntOk;
        private System.Windows.Forms.Button bntCancelar;
        private System.Windows.Forms.RadioButton rbnFIFO;
        private System.Windows.Forms.Label lblSequencia;
        private System.Windows.Forms.TextBox txtAddBloco;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvMP;
        private System.Windows.Forms.DataGridView dgvMC;
        private System.Windows.Forms.Button bntAddBloco;
        private System.Windows.Forms.DataGridViewTextBoxColumn Blocos;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn BlocosMC;
        private System.Windows.Forms.Label label2;
    }
}

