﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace SimuladorMC_AOC
{
    class simulador
    {
        private int tamanho_mp;
        private int tamanho_bloco;
        private int qtd_linhas;
        private bool tm_direta;
        private bool tm_associativa;
        private bool as_lru;
        private bool as_lfu;
        private bool as_fifo;

        public int getTamanho_mp()
        {
            return tamanho_mp;
        }

        public void setTamanho_mp(int v)
        {
            tamanho_mp = v;
        }

        public int getTamanho_bloco()
        {
            return tamanho_bloco;
        }

        public void setTamanho_bloco(int v)
        {
            tamanho_bloco = v;
        }

        public int getQtd_linhas()
        {
            return qtd_linhas;
        }

        public void setQtd_linhas(int v)
        {
            qtd_linhas = v;
        }

        public bool getTm_direta()
            {
                return tm_direta;
            }

        public void setTm_direta(bool v)
        {
                tm_direta = v;
            }

        public bool getTm_associativa()
            {
                return tm_associativa;
            }

        public void setTm_associativa(bool v)
        {
            tm_associativa = v;
            }

        public bool getAs_lru()
            {
                return as_lru;
            }

        public void setAs_lru(bool v)
        {
                as_lru = v;
            }
        
        public bool getAs_lfu()
            {
                return as_lfu;
            }

        public void setAs_lfu(bool v)
        {
                as_lfu = v;
            }

        public bool getAs_fifo()
            {
                return as_fifo;
            }

        public void setAs_fifo(bool v)
        {
                as_fifo = v;
            }

        public int CalcBlocoMp(int x, int y)
        {
            return (x / y);
        }

        public int potencia(int n)
        {

            if (n % 2 != 0) return -1;
            int contador = 1;
            while (n != 2)
            {
                n = n / 2;
                if (n % 2 == 0)
                {
                    contador++;
                    continue;
                }
                else return -1;
            }

            return contador;
        }
    }
}