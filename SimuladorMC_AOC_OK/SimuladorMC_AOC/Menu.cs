﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using SimuladorMC_AOC;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimuladorMC_AOC
{
    public partial class Formmenu : Form
    {
        public Formmenu()
        {
            InitializeComponent();
        }
        simulador s = new simulador();

        string msg = "";
        int p, b, l, tmp, bloco, mod, fifo, lfutroca = 0;
        int[] mc, contmc;
        bool[] mp;
        List<int> lru, lfu;

        private void bntCancelar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void txtTMP_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
            {
                e.Handled = true;
            }
        }

        private void txtTBloco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
            {
                e.Handled = true;
            }
        }

        private void txtQtdLinha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
            {
                e.Handled = true;
            }
        }

        private void txtAddBloco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
            {
                e.Handled = true;
            }
        }

        private void bntAddBloco_Click(object sender, EventArgs e)
        {
            if (txtAddBloco.Text != "")
            {
                bloco = Convert.ToInt32(txtAddBloco.Text);
                txtAddBloco.Text = "";
                txtAddBloco.Focus();
                if (bloco >= tmp)
                {
                    MessageBox.Show("Números válidos de 0 até " + (tmp - 1));
                }
                else 
                {
                    lblSequencia.Text += "  " + bloco;
                    if (rbnDireta.Checked) //-------------------------------------------------------------------------------------------------------Direta
                    {
                        if (!mp[bloco]) // verifica se o bloco esta na mc, true sim, false nao
                        {
                            dgvMC.Rows.Clear();
                            mp[bloco] = true;
                            mod = bloco % s.getQtd_linhas(); // faz o mod

                            if (mc[mod] == -1) // se a posição mc[mod] esta vazia, se sim coloca o bloco na mc
                            {
                                mc[mod] = bloco;
                                Console.WriteLine("Bloco " + bloco + " foi para copiado para Cache.");
                            }
                            else // senao o bloco e substituido
                            {
                                mp[mc[mod]] = false; // marca o bloco que esta na cache na posicao mc[mod] na MP como falso
                                Console.WriteLine("Bloco " + mc[mod] + " foi substituído pelo bloco " + bloco);
                                mc[mod] = bloco; // substitui
                            }
                            for (int i = 0; i < mc.Length; i++) // exibe o vetor no grid
                            {
                                if (mc[i] != -1)
                                    dgvMC.Rows.Add(i, "Bloco " + mc[i]);
                                else
                                    dgvMC.Rows.Add(i, "");
                            }
                        }
                        else // nao faz nada, tem o else por causa do console
                        {
                            Console.WriteLine("O Bloco " + bloco + " já está na Cache.");
                        }

                    }
                    else if (rbnAssociativa.Checked) //-----------------------------------------------------------------------------------------Associativa
                    {
                        if (rbnLRU.Checked) //-------------------------------------------------------------------------------------------------------LRU
                            if (!mp[bloco]) // verifica se o bloco esta na mc, true sim, false nao
                            {
                                dgvMC.Rows.Clear();
                                mp[bloco] = true;
                                if (lru.Count() != mc.Length) //se a lista tiver a mesma quantidade do mc.Length tem que substituir o bloco
                                {
                                    for (int i = 0; i < mc.Length; i++)
                                    {
                                        if (mc[i] == -1) // adiciona o bloco enquanto a mc não esta cheia
                                        {
                                            lru.Add(bloco);
                                            mc[i] = bloco;
                                            Console.WriteLine("Bloco " + bloco + " foi para copiado para Cache.");
                                            break;// para o for
                                        }
                                    }
                                }
                                else
                                {
                                    fifo = lru[0];
                                    lru.RemoveAt(0);
                                    mp[fifo] = false; //marca o bloco que saiu como falso
                                    for (int j = 0; j < mc.Length; j++)
                                    {
                                        if (mc[j] == fifo) // quando o bloco da mc for igual ao lru[0]"primeiro item da lista, Sempre vai ser o ultimo recentemente usado" 
                                        {
                                            lru.Add(bloco); // substitui o bloco
                                            mc[j] = bloco;
                                            Console.WriteLine("Bloco " + fifo + " foi substituído pelo bloco " + bloco);
                                        }
                                    }
                                }
                                for (int q = 0; q < mc.Length; q++) // exibe no grid
                                {
                                    if (mc[q] != -1)
                                    {
                                        dgvMC.Rows.Add(q, "Bloco " + mc[q]);
                                    }
                                    else
                                    {
                                        dgvMC.Rows.Add(q, "");
                                    }
                                }
                            }
                            else
                            {
                                if (lru.Contains(bloco)) //verifica se o bloco esta na lista
                                {
                                    for (int i = 0; i < lru.Count(); i++)
                                    {
                                        if (lru[i] == bloco) // remove o bloco da sua posição e isere ele no final da lista
                                        {
                                            lru.Remove(lru[i]);
                                            lru.Add(bloco);
                                            Console.WriteLine("O Bloco " + bloco + " já está na Cache.");
                                            break;// para o for
                                        }
                                    }
                                }
                            }
                        else if (rbnLFU.Checked) //-------------------------------------------------------------------------------------------------------LFU
                        {
                            int i = 0, lfumin = 99999, contmin = 0; // variaveis para usar abaixo
                            dgvMC.Rows.Clear();
                            if (!mp[bloco]) // verifica se o bloco ja esta na MC. TRUE sim, False nao  
                            {
                                mp[bloco] = true;

                                if (lfu.Count() != mc.Length) // se a lista tiver a mesma quantidade do mc.Length tem que substituir o bloco
                                {
                                    for (i = 0; i < mc.Length; i++)
                                    {
                                        if (mc[i] == -1) // add o bloco quando a MC não está cheia
                                        {
                                            lfu.Add(bloco);
                                            mc[i] = bloco;
                                            contmc[i]++;
                                            Console.WriteLine("Bloco " + bloco + " foi para copiado para Cache.");
                                            break; // para o for
                                        }
                                    }
                                }
                                else
                                {
                                    for (int j = 0; j < mc.Length; j++)
                                    {
                                        if (contmc[j] < lfumin) // acha o bloco menos referenciado, com menor contmc[x]
                                        {
                                            lfumin = contmc[j];
                                            contmin++;
                                        }
                                    }
                                    for (int j = 0; j < lfu.Count(); j++)
                                    {
                                        for (int l = 0; l < mc.Length; l++)
                                        {
                                            if (lfu[j] == mc[l] && contmc[l] == lfumin) // substitui o bloco 
                                            {
                                                lfu.Remove(lfu[j]);
                                                mp[mc[l]] = false;
                                                Console.WriteLine("Bloco " + mc[l] + " foi substituído pelo bloco " + bloco);
                                                mc[l] = bloco;
                                                lfu.Add(bloco);
                                                contmc[l] = 1;
                                                lfutroca = 1;
                                                break; // para o for da cache
                                            }
                                        }
                                        if (lfutroca == 1)
                                        {
                                            break; //para o for da lista
                                        }
                                    }
                                    lfutroca = 0;
                                }
                            }
                            else // se o bloco ja ta na MC add mais 1 no cont, pq ele foi referenciado
                            {
                                for (int q = 0; q < mc.Length; q++) 
                                {
                                    if (mc[q] == bloco)
                                    {
                                        contmc[q]++;
                                        Console.WriteLine("O Bloco " + bloco + " já está na Cache, foi referenciado " + contmc[q] + " vezes.");
                                        break;
                                    }
                                }
                            }
                            for (int q = 0; q < mc.Length; q++) // exibe no grid
                            {
                                if (mc[q] != -1)
                                {
                                    dgvMC.Rows.Add(q, "Bloco " + mc[q]);
                                }
                                else
                                {
                                    dgvMC.Rows.Add(q, "");
                                }
                            }
                        }
                        else if (rbnFIFO.Checked) //-------------------------------------------------------------------------------------------------------FIFO
                        {
                            Fila f = Fila.getInstance(s.getQtd_linhas() + 1);
                            if (!mp[bloco]) // verifica se o bloco ja ta na Cache, se true ele ta, se false add na Cache
                            {
                                dgvMC.Rows.Clear();
                                mp[bloco] = true; // marca o bloco na MP como que esta na MC
                                if (!f.cheia()) // se a fila tiver cheia, quer dizer que a MC ta cheia. Enquanto não tiver cheia
                                {
                                    for (int i = 0; i < mc.Length; i++)
                                    {
                                        if (mc[i] == -1) // adiciona o bloco quando ainda tiver linha vazia = -1
                                        {
                                            f.enfileirar(bloco);
                                            mc[i] = bloco;
                                            Console.WriteLine("Bloco " + bloco + " foi para copiado para Cache.");
                                            break;
                                        }
                                    }
                                }
                                else // so substitui quando tiver cheia
                                {
                                    fifo = f.desenfileirar(); //tira o 1 da fila 
                                    mp[fifo] = false; // "retorna" para memoria principal
                                    for (int j = 0; j < mc.Length; j++)
                                    {
                                        if (mc[j] == fifo) // tira o bloco do vetor que eu tirei da fila
                                        {
                                            f.enfileirar(bloco); // coloca o bloco novo
                                            mc[j] = bloco;
                                            Console.WriteLine("Bloco " + fifo + " foi substituído pelo bloco " + bloco);
                                        }
                                    }
                                }
                                for (int q = 0; q < mc.Length; q++) //exibe o vetor no grid
                                {
                                    if (mc[q] != -1)
                                    {
                                        dgvMC.Rows.Add(q, "Bloco " + mc[q]);
                                    }
                                    else
                                    {
                                        dgvMC.Rows.Add(q, "");
                                    }
                                }
                            }
                            else // nao faz nada
                            {
                                Console.WriteLine("O Bloco " + bloco + " já está na Cache.");
                            }
                        }
                    }
                }
            }
            else
            {
                txtAddBloco.Focus();
            }
        }

        private void bntOk_Click(object sender, EventArgs e)
        {
            msg = "";
            bntAddBloco.Enabled = false;
            dgvMP.Rows.Clear();
            dgvMC.Rows.Clear();
            Console.WriteLine("*_* *_* *_* *_* *_* *_* *_* *_* *_* *_* *_* ");
            lblSequencia.Text = "Blocos mapeados:";
            if (txtTMP.Text != msg && txtTBloco.Text != msg && txtQtdLinha.Text != msg)
            {
                p = Convert.ToInt32(txtTMP.Text);
                b = Convert.ToInt32(txtTBloco.Text);
                l = Convert.ToInt32(txtQtdLinha.Text);

                if (s.potencia(p) == -1)
                {
                    msg += "Tamanho da Memória principal inválido! \n";
                    txtTMP.Focus();
                }
                if (s.potencia(b) == -1)
                {
                    msg += "Tamanho do Bloco inválido! \n";
                }
                if (s.potencia(l) == -1)
                {
                    msg += "Quantidade de Linhas inválido! \n";
                }

                if (rbnDireta.Checked)
                {
                    s.setTm_direta(true);
                }
                else if (rbnAssociativa.Checked)
                {
                    s.setTm_associativa(true);
                    if (rbnLRU.Checked)
                    {
                        s.setAs_lru(true);
                    }
                    else if (rbnLFU.Checked)
                    {
                        s.setAs_lfu(true);
                    }
                    else if (rbnFIFO.Checked)
                    {
                        s.setAs_fifo(true);
                    }
                    else
                    {
                        msg += "Selecione um Algoritimo de Substituição!\n";
                    }
                }
                else
                {
                    msg += "Selecione um método de Mapeamento!\n";
                }

                if (msg != "")
                {
                    MessageBox.Show(msg);
                }
                else
                {
                    s.setTamanho_mp(p);
                    s.setTamanho_bloco(b);
                    s.setQtd_linhas(l);

                    bntAddBloco.Enabled = true;

                    Fila f = Fila.Destruir();

                    tmp = s.CalcBlocoMp(s.getTamanho_mp(), s.getTamanho_bloco()); // calcula o tamanho do bloco



                    mp = new bool[tmp];

                    for (int i = 0; i < mp.Length; i++)
                    {
                        dgvMP.Rows.Add("Bloco " + i);
                    }
                    lru = new List<int>(s.getQtd_linhas());
                    lfu = new List<int>(s.getQtd_linhas());
                    mc = new int[s.getQtd_linhas()];
                    contmc = new int[s.getQtd_linhas()];

                    for (int i = 0; i < mc.Length; i++)
                    {
                        dgvMC.Rows.Add(i, "");
                        mc[i] = -1; // inicia o vetor mc com os valores -1
                        contmc[i] = 0;
                    }
                }
            }
            else
            {
                MessageBox.Show("Preencha todos os campos!");
            }
        }
    }
}
